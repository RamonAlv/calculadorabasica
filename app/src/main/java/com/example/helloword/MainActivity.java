package com.example.helloword;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView txtCount;
    private EditText Etxt1, Etxt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCount = (TextView) findViewById(R.id.txtCount);
        txtCount.setText("0");
        Etxt1 = (EditText) findViewById(R.id.Etxt1);
        Etxt2 = (EditText) findViewById(R.id.Etxt2);
    }

    public void OnClick(View view){

        Button bt = (Button)view;
        double Valor1;
        double Valor2;
        //txtCount.setText(bt.getText().toString());
        switch (bt.getText().toString()){
            case "+":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + (Valor1 + Valor2));
                }else
                    Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                Etxt1.setText("");
                Etxt2.setText("");
                break;
            case "-":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + (Valor1 - Valor2));
                }else
                    Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                Etxt1.setText("");
                Etxt2.setText("");
                break;
            case "*":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + (Valor1 * Valor2));
                }else
                    Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                Etxt1.setText("");
                Etxt2.setText("");
                break;
            case "/":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("") && !Etxt2.getText().toString().equals("0")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + (Valor1 / Valor2));
                }else {
                    if(Etxt2.getText().toString().equals("0"))
                        Toast.makeText(this, "No se puede dividir nada entre 0", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                }
                Etxt1.setText("");
                Etxt2.setText("");
                break;
            case "%":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + (Valor1 * (Valor2/100)) + "%");
                }else
                    Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                Etxt1.setText("");
                Etxt2.setText("");
                break;
            case "^":
                if(!Etxt1.getText().toString().equals("") && !Etxt2.getText().toString().equals("")) {
                    Valor1 = Double.parseDouble(Etxt1.getText().toString());
                    Valor2 = Double.parseDouble(Etxt2.getText().toString());
                    txtCount.setText("" + Math.pow(Valor1, Valor2));
                }else
                    Toast.makeText(this, "Necesita rellenar los espacios en blanco", Toast.LENGTH_SHORT).show();
                Etxt1.setText("");
                Etxt2.setText("");
                break;
        }
    }
}
